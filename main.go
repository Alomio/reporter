package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"image/jpeg"
	"io/ioutil"
	"log"

	"gocv.io/x/gocv"
)

// ImageFrame Payload that will be sent to the cloud
type ImageFrame struct {
	Title  string `json:"Title"`
	Author string `json:"author"`
	Link   string `json:"link"`
}

func main() {

	var cameraID int

	flag.IntVar(&cameraID, "camera", 0, "The ID of the camera to be used for image capture")
	flag.Parse()

	webcam, err := gocv.OpenVideoCapture(cameraID)
	if err != nil {
		fmt.Printf("Error opening video capture device: %v\n", cameraID)
		log.Fatal(err)
	}
	defer webcam.Close()

	frame := gocv.NewMat()
	defer frame.Close()
	webcam.Read(&frame)
	if ok := webcam.Read(&frame); !ok {
		fmt.Printf("cannot read device %v\n", cameraID)
		return
	}
	if frame.Empty() {
		fmt.Printf("no image on device %v\n", cameraID)
		return
	}

	// from gocv.Mat to image.Image
	img, err := frame.ToImage()
	if err != nil {
		log.Fatal(err)
	}
	// Need a byte array to convert to base64
	// Encode it as a jpg str in memory
	buf := new(bytes.Buffer)
	jpeg.Encode(buf, img, nil)
	//	if err != nil {
	//		fmt.Println("failed to create buffer", err)
	//	}
	// var someByteString []byte
	// convert buffer to reader
	reader := bytes.NewReader(buf.Bytes())
	// Read contents back out to a string
	contents, err := ioutil.ReadAll(reader)
	if err != nil {
		fmt.Println("failed to read buffer", err)
	}

	encodedImage := base64.StdEncoding.EncodeToString(contents)
	samplePayload := &ImageFrame{
		Title:  "Example",
		Author: "Luke",
		Link:   "www.google.com",
	}

	exampleJSON, _ := json.Marshal(samplePayload)
	//fmt.Println(string(exampleJSON))
	//fmt.Println("ENCODING:\n" + encodedImage)
}

// Longer ish tutorial on encoding images: https://freshman.tech/snippets/go/image-to-base64/
