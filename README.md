# Reporter

A multi-threaded image reporter.  Take Photos and either stream them back or send image snap shots to object storage. 

## Install
To install you must have OpenCV and GoCV. Both can be installed at [GoCV.io]

## Use

```
go run hello.go
```

## Test
It's static so it'll yell at you if it doesn't work.

<!-- Links for sections above -->
![GoCV.io]: https://gocv.io/getting-started/linux/
